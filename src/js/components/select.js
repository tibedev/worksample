
export default {
    _toggles: null,
    _targets: null,
    _activeString: '--active',
    init() {
        this._toggles = document.querySelectorAll('[data-toggle]');
        this._targets = document.querySelectorAll('[data-toggle-target]');
        if (this._toggles.length && this._targets.length) {
            Array.prototype.forEach.call(this._toggles, toggleElement => {
                const targetId = toggleElement.getAttribute('data-toggle');
                let targetElement;
                Array.prototype.forEach.call(this._targets, (el) => {
                    if (el.getAttribute('data-toggle-target') === targetId) {
                        targetElement = el;
                        return;
                    }
                });
                toggleElement.addEventListener(('click'), () => {
                    if (targetElement) {
                        toggleElement.classList.toggle(
                            toggleElement.getAttribute('data-toggle-class') + this._activeString);
                        targetElement.classList.toggle(
                            targetElement.getAttribute('data-toggle-class') + this._activeString);
                    }
                })
            });
        }
    }
}