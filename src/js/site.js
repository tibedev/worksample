import * as components from './components/*.js';

const app = {
	init() {
		for (let component in components) {
			if (!components.hasOwnProperty(component) ||
					!components[component] ||
					(typeof components[component] !== 'object')) {
				continue;
			}

			let componentName = component;
			component         = components[componentName];
			if (component.hasOwnProperty('init')) {
				component.init();
			}
		}
	}
};

window.onload = () => {
	app.init();
}