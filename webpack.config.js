
const path = require('path');
const glob = require('glob');
const BrowserSync = require('browser-sync-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const RemoveStylesJSFiles = require('webpack-fix-style-only-entries');

function scanDir(pattern) {
	let sources = {};

	glob.sync(pattern).forEach((file) => {
		let filename = file.replace(/.+\//, '').split('.')[0];
		if( !sources.hasOwnProperty(filename) ){
			sources[filename] = [];
		}
		sources[filename].push(path.join(__dirname, file));
	});

	return sources;
}

const paths = {
  	src: 'src',
	dist: 'dist',
	jsDir: 'js',
	scssDir: 'scss',
	cssDir: 'css',
	js: '*.js',
	scss: '*.scss'
};

const pattern = `${paths.src}/*(${paths.jsDir}|${paths.scssDir})/*(${paths.js}|${paths.scss})`;

module.exports = (env, argv) => {
	const isDevMode = argv.mode === 'development';

	return {
		entry : scanDir(pattern),
		output : {
			path: path.resolve(__dirname, `${paths.dist}`),
			filename: `${paths.jsDir}/[name].min.js`
		},
		devtool: isDevMode ? 'inline-source-map' : false,
		stats : {
			children: false
		},
		module : {
			rules: [
				{
					// sass / scss loader for webpack
					test : /\.(sass|scss)$/,
					use: [
						{
							loader: MiniCssExtractPlugin.loader
						},
						{
							loader : 'css-loader',
							options: {
								sourceMap  : isDevMode,
								importLoaders: 1
							}
						},
						{
							loader : 'postcss-loader',
							options: {
								ident  : 'postcss',
								sourceMap: isDevMode,
								plugins : [
									require('autoprefixer')()
								]
							}
						}, 
						{
							loader : 'sass-loader',
							options: {
								outputStyle: 'compressed',
								sourceMap : isDevMode
							}
						}
					]
				}, 
				{
					// JS
					test  : /\.js$/,
					exclude: /(node_modules|bower_components)/,
					use  : [
						{
							loader: 'babel-loader',
							options: {
								plugins: [
									['import-glob']
								]
							}
						},
						{
							loader : 'babel-loader',
							options: {
								presets: [
									['@babel/preset-env', {
										loose   : true,
										modules  : false,
										useBuiltIns: 'usage'
									}],
									'minify'
								],
								plugins: [
									['transform-strict-mode']
								]
							}
						}
					]
				}
			]
		},
		plugins: [
			new BrowserSync({
				server: {
					baseDir: "./dist"
				}
			}),
			new RemoveStylesJSFiles({
				silent: true
			}),
			new MiniCssExtractPlugin({
				filename: `${paths.cssDir}/[name].min.css`,
			})
		],
	};
}